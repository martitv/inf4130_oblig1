import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Scanner;

class Exercise1{
    String outputFile;
    PrintWriter writer;
    public static void main(String args[]){
        Exercise1 ex = new Exercise1();
        ex.readFile(args[0], args[1]);
    }
    public void readFile(String input, String output){
        outputFile = output;
        try {
            writer = new PrintWriter(outputFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        File file = new File(input);
        int n;
        int k;
        int[] series;

        try {

            Scanner sc = new Scanner(file);

            while (sc.hasNextLine()) {
                n = sc.nextInt();
                k = sc.nextInt();
                series = new int[n];
                for(int i = 0; i < n; i++){
                    series[i] = sc.nextInt();
                }

                writer.printf("INSTANCE %d %d", n, k);
                //writer.print(n + " " + k);
                for(int i = 0; i < n; i++){
                    writer.print(" " + series[i]);
                }
                writer.println();

                Arrays.sort(series);
                sos(n, k, series);
            }
            sc.close();
            writer.close();
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void sos(int n, int k, int[] series){
        int[] u = new int[n];
        int sum = k;




        if(sum == 0){
            writeFile("YES", u, series);
        }else{
            writeFile("NO", u, series);
        }

    }
    public void writeFile(String yn, int[] used, int[] series){

        if(yn.equals("YES")) {
            writer.print(yn + " ");
            for (int i = 0; i < used.length; i++) {
                if (used[i] > 0) {
                    writer.printf("(%d,%d)", series[i], 1);
                }else{
                    writer.printf("(%d,%d)", series[i], 0);
                }
            }
            writer.println("\n");
        }else{
            writer.println(yn + "\n");
        }

    }
}